import React, { Dispatch } from "react";
import { useDispatch } from "react-redux";

import { ThemeActions } from "../../../redux/actions/themeActions";
import { ThemeTypes } from "../../../shared/constants";

import { FontActions } from "../../../redux/actions/fontActions";
import { FontTypes } from "../../../shared/constants";

const ButtonRedux: React.FC = () => {
  const themeDispatch = useDispatch<Dispatch<ThemeActions>>();
  const fontDispatch = useDispatch<Dispatch<FontActions>>();

  const handleSetTheme = () => {
    themeDispatch({ type: "SET_THEME", payload: ThemeTypes.dark });
  };

  const handleSetFont = () => {
    fontDispatch({ type: "SET_FONT", payload: FontTypes.huge });
  };

  return (
    <React.Fragment>
      <button onClick={() => handleSetTheme()}>Change Theme</button>
      <button onClick={() => handleSetFont()}>Change Font</button>
    </React.Fragment>
  );
};

export default ButtonRedux;
