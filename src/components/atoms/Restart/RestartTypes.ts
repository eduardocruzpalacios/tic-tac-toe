export type RestartValue = 'Play again';

export interface RestartProps {
  value: RestartValue;
  handleOnClick: React.MouseEventHandler<HTMLButtonElement>;
}
