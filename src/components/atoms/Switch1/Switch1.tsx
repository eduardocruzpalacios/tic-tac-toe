import React from "react";

import { useGlobalSizes, Sizes } from "../../../shared/context";

const Switch1: React.FC = () => {
  const { setSize, size } = useGlobalSizes();

  const setSizeFunc = () => {
    if (setSize) {
      setSize(size === Sizes.SSize ? Sizes.LSize : Sizes.SSize);
    }
  };

  return (
    <div>
      <input onChange={setSizeFunc} type="checkbox" />
      <label>
        <span>Switch button</span>
      </label>
    </div>
  );
};

export default Switch1;
