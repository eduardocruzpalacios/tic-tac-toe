import React from "react";

import "./Square.scss";

import * as Types from "./SquareTypes";

const Square: React.FC<Types.SquareProps> = ({
  key,
  value,
  handleOnClick,
}: Types.SquareProps) => (
  <React.Fragment>
    <button className="square" onClick={handleOnClick} key={key}>
      {value && value}
    </button>
  </React.Fragment>
);

export default Square;
