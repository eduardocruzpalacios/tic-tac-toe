import React, { useState, useEffect, useMemo } from "react";
import Square from "../../atoms/Square/Square";
import Restart from "../../atoms/Restart/Restart";
import Count from "../../atoms/Count/Count";
import SymbolZ from "../../atoms/SymbolZ/SymbolZ";
import ButtonRedux from "../../atoms/ButtonRedux/ButtonRedux";

// REDUX
import { useSelector } from "react-redux";
import { AppState } from "../../../redux/reducers/rootReducer";

import "./Game.scss";

interface IStateIsXNext {
  isXNextValue?: boolean;
}

interface ISymbol1 {
  Symbol1Value: string;
}

interface ISymbol2 {
  Symbol2Value: string;
}

interface IPrevSymbol1 {
  PrevSymbol1Value: string;
}

interface IPrevSymbol2 {
  PrevSymbol2Value: string;
}

const Game: React.FC = () => {
  const [squares, setSquares] = useState(Array(9).fill(""));

  const [isXNext, setIsXNext] = useState<IStateIsXNext>({ isXNextValue: true });
  const { isXNextValue } = isXNext;

  const [symbol1, setSymbol1] = useState<ISymbol1>({ Symbol1Value: "X" });
  const [symbol2, setSymbol2] = useState<ISymbol2>({ Symbol2Value: "O" });
  const { Symbol1Value } = symbol1;
  const { Symbol2Value } = symbol2;
  const nextSymbol = isXNextValue ? Symbol1Value : Symbol2Value;

  // to store prev symbols values
  const [prevSymbol1, setPrevSymbol1] = useState<IPrevSymbol1>({
    PrevSymbol1Value: "X",
  });
  const [prevSymbol2, setPrevSymbol2] = useState<IPrevSymbol2>({
    PrevSymbol2Value: "O",
  });
  const { PrevSymbol1Value } = prevSymbol1;
  const { PrevSymbol2Value } = prevSymbol2;

  const winner = calculateWInner(squares);

  const [count, setCount] = useState(1);

  //redux
  const { theme } = useSelector((state: AppState) => state.theme);

  const { font } = useSelector((state: AppState) => state.font);

  useEffect(() => {
    console.log({ count });
    document.title = `Game ${count}`;
  }, [count]);

  useMemo(() => updateMatrix(), [prevSymbol1, prevSymbol2]);

  function updateMatrix() {
    console.log("updateMatrix()");

    console.log("squares ANTES");
    console.log(squares);

    const updatedSquares = squares.slice();
    console.log("updatedSquares ANTES");
    console.log(updatedSquares);

    console.log("BUCLE init");
    for (let i = 0; i < squares.length; i++) {
      //console.log("loop " + (Number(i) + 1));
      if (updatedSquares[i] === PrevSymbol1Value) {
        console.log("PrevSymbol1Value " + PrevSymbol1Value);
        console.log("Symbol1Value " + Symbol1Value);
        updatedSquares[i] = Symbol1Value;
      } else if (updatedSquares[i] === PrevSymbol2Value) {
        console.log("PrevSymbol2Value " + PrevSymbol2Value);
        console.log("Symbol2Value " + Symbol2Value);
        updatedSquares[i] = Symbol2Value;
      }
    }
    console.log("BUCLE exit");

    console.log("updatedSquares DESPUÉS");
    console.log(updatedSquares);

    setSquares(updatedSquares);

    console.log("squares DESPUÉS");
    console.log(squares);
  }

  function renderSquare(i: number) {
    return (
      <React.Fragment>
        <Square
          key={i}
          value={squares[i]}
          handleOnClick={() => {
            if (squares[i] !== "" || winner != null) {
              return;
            }

            const nextSquares = squares.slice();
            nextSquares[i] = nextSymbol as string;
            setSquares(nextSquares);

            setIsXNext({ ...isXNext, isXNextValue: !isXNextValue });
          }}
        />
      </React.Fragment>
    );
  }

  function renderRestart() {
    return (
      <React.Fragment>
        <Restart
          value="Play again"
          handleOnClick={() => {
            setSquares(Array(9).fill(""));
            setIsXNext({ isXNextValue: true });
            setCount(count + 1);
          }}
        />
      </React.Fragment>
    );
  }

  function renderCount() {
    return (
      <React.Fragment>
        <h3>
          Current game:
          <Count value={count} />
        </h3>
      </React.Fragment>
    );
  }

  function renderSymbolZ(symbol: string, i: number, pj1: boolean) {
    return (
      <React.Fragment>
        <SymbolZ
          value={symbol}
          lenght={i}
          action={(event: any) => {
            if (pj1) {
              changeToken(true, event.target.value);
            } else {
              changeToken(false, event.target.value);
            }
            updateMatrix();
          }}
        />
      </React.Fragment>
    );
  }

  function changeToken(pj1: boolean, value: string) {
    if (pj1) {
      console.log("change pj1 token");
      setPrevSymbol1({
        ...prevSymbol1,
        PrevSymbol1Value: Symbol1Value,
      });
      setSymbol1({ ...symbol1, Symbol1Value: value });
      console.log("prev " + PrevSymbol1Value);
      console.log("current " + Symbol1Value);
    } else {
      console.log("change pj2 token");
      setPrevSymbol2({
        ...prevSymbol2,
        PrevSymbol2Value: Symbol2Value,
      });
      setSymbol2({ ...symbol2, Symbol2Value: value });
      console.log(PrevSymbol2Value);
      console.log(Symbol2Value);
    }
  }

  function getStatus() {
    let msg: string = "";
    if (winner) {
      msg += "Winner: " + winner;
    } else if (isBoardFull(squares)) {
      msg += "Draw!";
    } else {
      msg += "Next player: " + nextSymbol;
    }
    return msg;
  }

  return (
    <React.Fragment>
      <div className="game-info">{getStatus()}</div>
      <p>Choose players' tokens:</p>
      <div className="input-box">
        {renderSymbolZ(Symbol1Value, 1, true)}
        {renderSymbolZ(Symbol2Value, 1, false)}
      </div>
      <div className="game-board">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
      <div className="restart-button">{renderRestart()}</div>
      <div className="count">{renderCount()}</div>
      <div>Theme: {theme}</div>
      <div>Font: {font}</div>
      <div style={{ color: theme, fontSize: font }}>
        Hola, esta caja es para probar Redux en TS
      </div>
      <ButtonRedux />
    </React.Fragment>
  );
};

function calculateWInner(squares: string[]) {
  const possibleLines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  // go over all possibly winning lines and check if they consist of only X's/only O's
  for (let i = 0; i < possibleLines.length; i++) {
    const [a, b, c] = possibleLines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

function isBoardFull(squares: string[]) {
  for (let i = 0; i < squares.length; i++) {
    if (squares[i] === "") {
      return false;
    }
  }
  return true;
}

export default Game;
