import React from "react";

import Switch1 from "../../atoms/Switch1/Switch1";
import ContextSelectedSize from "../../atoms/ContextSelectedSize/ContextSelectedSize";

const SwitchSelector: React.FC = () => (
  <React.Fragment>
    <div>
      <Switch1 />
    </div>
    <div>
      <ContextSelectedSize />
    </div>
  </React.Fragment>
);

export default SwitchSelector;
