import React from "react";
import ReactDOM from "react-dom";
import Dashboard from "./components/organisms/Dashboard/Dashboard";
import { Provider } from "react-redux";

import store from "./redux/store/store";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Dashboard />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
