import { createStore } from "redux";
import { devToolsEnhancer } from "redux-devtools-extension";

import rootReducer from "../reducers/rootReducer";

import { ThemeActions } from "../actions/themeActions";
import { ThemeState } from "../reducers/themeReducer";

import { FontActions } from "../actions/fontActions";
import { FontState } from "../reducers/fontReducer";

const store = createStore<
  { theme: ThemeState; font: FontState },
  ThemeActions | FontActions,
  null,
  null
>(rootReducer, devToolsEnhancer({}));

export default store;
