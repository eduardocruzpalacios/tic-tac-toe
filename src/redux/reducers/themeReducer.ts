import { ThemeActions } from "../actions/themeActions";

import { ThemeTypes } from "../../shared/constants";

export type ThemeState = {
  theme: ThemeTypes;
};

const initialState: ThemeState = {
  theme: ThemeTypes.bright,
};

const ThemeReducer = (
  state: ThemeState = initialState,
  action: ThemeActions
): ThemeState => {
  switch (action.type) {
    case "SET_THEME":
      return {
        ...state,
        theme: action.payload ? action.payload : initialState.theme,
      };
    default:
      return state;
  }
};

export default ThemeReducer;
