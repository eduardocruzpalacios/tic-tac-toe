import { combineReducers } from "redux";
import ThemeReducer from "./themeReducer";
import FontReducer from "./fontReducer";

const rootReducer = combineReducers({
  theme: ThemeReducer,
  font: FontReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
