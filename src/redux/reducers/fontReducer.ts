import { FontActions } from "../actions/fontActions";

import { FontTypes } from "../../shared/constants";

export type FontState = {
  font: FontTypes;
};

const initialState: FontState = {
  font: FontTypes.small,
};

const FontReducer = (
  state: FontState = initialState,
  action: FontActions
): FontState => {
  switch (action.type) {
    case "SET_FONT":
      return {
        ...state,
        font: action.payload ? action.payload : initialState.font,
      };
    default:
      return state;
  }
};

export default FontReducer;
